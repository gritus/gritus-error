function GritusError(message, data) {
    var error = new Error()

    this.name = 'GritusError'

    if (typeof message !== 'string') {
        data = message
        message = undefined
    }

    this.message = message
    this.data = typeof data !== 'undefined' ? data : null

    this.stack = error.stack
}

GritusError.prototype = new Error

GritusError.prototype.toString = function () {
    var message = typeof this.message !== 'undefined' ? ': ' + this.message : '',
        dataString = this.data ? ' ' + JSON.stringify(this.data) : ''
    return this.name + message + dataString
}

module.exports = GritusError
